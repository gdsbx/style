Код Стайл

Переменные:
- используем camel case 
	$MyFirstVar - неправильно
	$my_first_var - неправильно
	$myFirstVar - правильно
	
- первые символы должны отображать тип переменой, на английском языке
	$arResult, $obElement, $strName
	если из названия переменной явно какого она типа, то это условие можно опустить ($counter, $isInnerPage, $dbResult)
	
- названия должны отображать по максимуму логику использования переменной, но быть не очень длынными. Желательно на английском языке.
	$arResultMergeFirstAndSecondArray - неправильно
	$nSchyotchick  - неправильно

Функции и методы:
- используем camel case
- названия должны отображать логику работы. GetUserID, Add2Basket.
	функции которые возварщают результат принято называть через приставку Get, те которые должны что-то утсановить Set

Фигурные скобки, табуляция и пробелы:

<?

$ar = array(1, 2, 3);

$ar = array(
	"ВАСЯ" => 123,
	"ВАСЯ" => 123,
	"ВАСЯ" => 123,
	"Array" => array("213", "as"),
	"Array" => array(
		"213",
		"as",
		"213",
		"213",
		"213",
		"213",
		"asdasd"
		),
	"array" => array(
		"key" => "value"
		"key" => array(
			"innerKey" => "value",
			"innerKey" => "value",
		)
	)
	
);
if(true){
	
	$range = range(1, 10, 1);
	
	foreach($range as $key => $a){
		switch($a){
			case 1: 
				if(($a + 1) % 2 == 0){
					return array_merge(array("2", "3"), (array)$a);
				}
				else if(false){
					continue 2;
				}
				else {
					break 3;
				}	
			break;
			
			case 0:
			case 2:
			case 3:
				echo 3;
				break;
			
			default:
				echo 1;
			break;
		}
	}
}

for($i = 1; $i < 10; $i++){
	
}

MyFunction($a, $b, $c){
	$d = $a / $c;
	
	return $d * $b == 7 ? $a : array($d, $b);
}

$ob = new MyClass();
$name = $ob->GetName('eminem');
$name = $ob->__name;

$name = MyClass::GetFullName(
	"eminem",
	$APP->getCurUser(),
	true,
	true,
	false,
	17
);

Class MyClass{
	const ONLY_UPPER_CASE = 12;
	static protected $lol = array();
	private $id;
	
	function __construct($id){
		$this->id = $id;
		self::$lol[] = $this->id;
	}
	
	static private function Close($die = true){
		if($die){
			die('Goodbye!');
		}
	}
}

?>

если код используется в шаблоне, где представлен html, то обязательно используем альтернативный синтаксис

<?if(!empty($arResult["ITEMS"]) && $arParams["I_LOVE_YOU_PHP"] == "Y"):
	$counter = 0;
	$tempFunction  = function($a, $b){
		$c = 0;
		if($a > 0 && $b){
			$c = $a / $b;
			$c += 17;
			$c++;
		}
		else{
			$c = -1;
		}
		
		return $c;
	}
?>
	<ul>
	<?foreach($arResult["ITEMS"] as $arItem):
		$code = $tempFunction($arItem["ID"]);
	?>
		<li>
			<?if($arItem["LEARN_LINK_EDIT"]):?>
				<a href="http://php.net/manual/ru/index.php">
					<span style="font-size:20px">
						<?=$arItem["LEARN_LINK_EDIT"]?>
					<span>
				</a>
			<?else:?>
				text
			<?endif?>
		</li>
		<?
		unset($tempFunction, $code);
		break;
		?>
	<?endforeach;?>
	</ul>
<?endif?>
